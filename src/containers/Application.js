import React, { Component } from 'react';
import { Link, Route, NavLink } from 'react-router-dom';

import Page from '../components/Page';
import Speakers from '../components/Speakers';


// TODO - refactor navlinks with .map()
const navigationLinks = [
	{
		to: '/',
		text: 'Home'
	},
	{
		to: '/about',
		text: 'About'
	}
]


class Application extends Component {

	render() {	
		return (
			<div>

				<nav className="nav">
					<ul className="nav__menu">
						<li className="nav__item">
							
							<Link 
								className="nav__link"
								to="/"
							>Home</Link>

						</li>
						<li className="nav__item">
							
							<Link 
								className="nav__link"
								to="/about"
							>About</Link>

						</li>
						<li className="nav__item">
							
							<Link 
								className="nav__link"
								to="/speakers"
							>Speakers</Link>
							
						</li>

						<li className="nav__item">
							
							<Link 
								className="nav__link"
								to="/schmoozle"
							>Schmoozle</Link>
							
						</li>
					</ul>
				</nav>

				<main>

					<Route 
						path="/"
						exact
						render={() => (
							<Page name="home" />
						)}
					/>

					<Route 
						path="/about"
						render={() => (
							<Page name="about" />
						)}
					/>

					<Route 
						path="/schmoozle"
						render={() => (
							<Page name="schmoozle" />
						)}
					/>

					<Route path="/speakers" component={Speakers} />

				</main>
				
			</div>
		)
	};
}

export default Application;
