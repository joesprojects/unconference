import React, { Component, PropTypes } from 'react';
import { Link, Route } from 'react-router-dom';

class Page extends Component {

	static propTypes = {
		name: PropTypes.string,
		match: PropTypes.object,
	}

	state = {
		title: '...',
		content: '...'
	}

	componentDidMount() {
		this.getData();
	}

	getData = () =>{
		const { name } = this.props;

		if (name) {
			fetch(`mock/pages/${name}.json`)
			.then(response => {
				if (response.status == 200) {
					return response.json()
				}
			})
			.then(data => {
				this.setState({ 
					title: data.title,
					content: data.content
				});
			})
			.catch(error => {
				this.setState({ 
					title: 'Oh no!',
					content: `We couldn't find any data for ${name}`
				});
			});
		}
	}

	render() {
		const { title, content } = this.state;

		return (
			<section className="page">
				<h1 className="page__title">{title}</h1>
				<p className="page__intro">{content}</p>
			</section>
		)
	}
}

export default Page
