import React from 'react';
import { Link, Route } from 'react-router-dom';


const Speakers = ({ match }) => (
	<div className="speakers">
		<h2>Speakers</h2>
		<ul className="list--plain">
			<li>
				<Link to={`${match.url}/bill`}>Bill Jones</Link>
			</li>
			<li>
				<Link to={`${match.url}/jenny`}>Jenny Simpson</Link>
			</li>
		</ul>

		<Route path={`${match.url}/:speakerName`} component={Speaker} />

	</div>
)

const Speaker = ({ match }) => (
	<div>
		<h1>{match.params.speakerName}</h1>
	</div>
)

export default Speakers;