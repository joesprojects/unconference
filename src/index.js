import symbol from "core-js/es6/symbol";
require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route} from 'react-router-dom';

require('./scss/styles.scss');

import Application from './containers/Application';


ReactDOM.render(
	<HashRouter>
		<Route path="/" component={Application} />
	</HashRouter>,
	document.getElementById('root')
);
