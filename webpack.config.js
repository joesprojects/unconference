const path = require('path'),
	autoPrefixer = require('autoprefixer');

module.exports = {
	entry: './src/index.js',
	output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
	devtool: "source-map",
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react', 'stage-0'],
					plugins: [
						["transform-es2015-for-of", {
							"loose": true
						}]
					],
				}
			},
			{
				test: /\.scss$/,
				loaders: [
					'style-loader', 
					'css-loader', 
					{
						loader: 'postcss-loader',
						options: {
							plugins: () => {
								return [autoPrefixer]
							}
						}
					},
					'sass-loader'
				]
			}
		]
	}
};