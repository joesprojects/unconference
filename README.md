Hello! Thank you for attending this session on React Router. I hope you can use this technology in your career or that you enjoy building some software just for fun.

To run this project you need to run:

npm install

then

npm run serve



Once you've familiarised yourself, why not try making one or more of the changes below:


Pass data to individual speakers’ pages.

Add more Links and Routes, alter the content.

Experiment with different components from React Router such as NavLink or BrowserRouter.

Find out about different attributes you can pass to components, such as ‘exact’. 

Refactor the code:  break Application into smaller components and/or use .map() to cycle through links and pages.

Consider adding Redux a Redux store to the project. You don’t want to pass data down an endless line of child components!
